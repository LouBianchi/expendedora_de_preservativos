**Presentación:**
Este proyecto fue presentado como trabajo final para una materia de un secundario Técnico en programacion de software libre.
Trata de una máquina que, al subir un tweet con el hastag elegido, una cinta transportadora comienza a funcionar y te entrega un preservativo.

**Enlace de la presentación del proyecto e información:**
https://docs.google.com/presentation/d/12KnbCVOahJYxfV0SqbFFajWN368WaeviLsAfjjIOF1c/edit?usp=sharing


**Máquina:**
La máquina esta echa con los elementos que vienen en el kit de arduino educabot. Y para armarla vas a precisar:
*  Placa Arduino educabot.
*  Placa controladora de motores.
*  2 motores DC.
*  2 ruedas para sostener la cinta.
*  Cinta transportadora (o algo que asemeje a una).
*  Una estructura solida para sostener los motores con la cinta.
*  Una estructura en forma de caja donde quepa la maquina y otra en forma de tubo para poner los preservativos.

Los pines arduino estan configurados en 2 y 3-


**Cómo configurar:**
*  Obtener una cuenta de twitter en modo desarrollador, Twitter tardara aproximadamente 2 días en validar la cuenta.
*  En app/controllers/twitter_controller.rb encontraras la funcion consultar_twitter, hay deberas agregar las claves que te dara twitter una vez confirmada la
cuenta y pondras en hastag que quieras utilizar. Por ejemplo nosotros utiliamos el #CuidarseGarpa.
*  En el mismo archivo se encentra la función enviar_serial, en la que pondras el puerto arduino que corresponda a tu computadora.
*  Debes levantar el proyecto Rails (bundle install, migrate, rails s)
*  Conecta todo por usb y dale los permisos (arduino y rails)
*  Tenes unas vista a la que podras acceder desde la web donde mostrará los tweets enviados.


**Cómo contribuir:**
En los issues se escuentran cosas que no pudimos mejorar del proyecto. Si haces algún cambio te damos las gracias y no olvides hacer pull request para merge a master.