Rails.application.routes.draw do
  get '/', to: 'twitter#consultar_twitter'
  get '/json', to: 'twitter#leer_json'
  get '/led', to: 'twitter#prender_led'
end
