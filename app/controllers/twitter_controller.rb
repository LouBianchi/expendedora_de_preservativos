class TwitterController < ApplicationController
  def consultar_twitter
    client = Twitter::REST::Client.new do |config|
      config.consumer_key    = "*****"
      config.consumer_secret = "*****"
      config.access_token        = "*****"
      config.access_token_secret = "****"
    end
    hashtag = "#Nombre_Del_Hastag"

    @activoArduino = false
    resultados = client.search(hashtag)
    resultados.each do |apiTweet|
      tweet = Tweet.find_by_tweet_id(apiTweet.id)
      if (!tweet) #si todavía no estaba guardado, es nuevo
        self.guardar_tweet(apiTweet)
        self.enviar_serial
        @activoArduino = true
      end

    end
    @tweets = Tweet.all
  end
  def guardar_tweet(apiTweet)
    tweet = Tweet.new
    tweet.tweet_id = apiTweet.id
    tweet.user_id = apiTweet.user.id
    tweet.user_screen_name = apiTweet.user.screen_name
    tweet.tweet_created_at = apiTweet.created_at #.to_datetime
    tweet.tweet_text = apiTweet.text
    tweet.save
  end

  def enviar_serial
    port_str = "/dev/ttyACM0"
    baud_rate = 9600
    data_bits = 8
    stop_bits = 1
    parity = SerialPort::NONE
    if port_connected?(port_str)
      sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
      sp.write('a')
      sp.flush
    end
  end
  private
    def port_connected?(port)
      return true if Dir.glob(port).count == 1
    end
end
