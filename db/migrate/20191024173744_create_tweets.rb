class CreateTweets < ActiveRecord::Migration[5.2]
  def change
    create_table :tweets do |t|
      t.bigint :tweet_id
      t.bigint :user_id
      t.string :user_screen_name
      t.datetime :tweet_created_at
      t.text :tweet_text
      t.datetime :delivered_to_arduino

      t.timestamps
    end
  end
end
